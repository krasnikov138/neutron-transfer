#include <fstream>
#include <string>
#include <array>
#include <tuple>
#include <map>

#include "grid.hpp"

#ifndef SOLVER_CLASS
#define SOLVER_CLASS

struct CellSize {
	double a, b, c;
	CellSize(double a = 1, double b = 1, double c = 1): a(a), b(b), c(c) {}
};

class Solver {
	friend class Characteristic;
private:
	/* type of geometry: 0 - rectangular, 1 - hexagonal */
	/* is used only when the result is saved to output file */
	uint8_t geometry;
protected:
	/* number of cells for each coordinate */
	int N, M, L;
	/* the sizes of the pattern cell */
	CellSize cell_size;
	/* lattice with distribution function values */
	int num_grids;
	Grid* grid;

	/* source function */
	double (*q_ext)(double x, double y, double z);
	/* scatter crossection */
	double sigma;
	/* wall reflection coefficient */
	double reflection;

	/* defines for directions transform in each other during reflections */
	uint8_t *reflection_matrix;
	/* store the parameters of characteristics (type and direction) */
	Characteristic *chars;

	/* for test purposes */
	std::map<size_t, double> input_streams;
	size_t get_stream_index(int dir_num, int grid_ind, const Index& cell);

	/* function which calculates point number in considering geometry */
	virtual uint8_t get_point(const Vector &vec) = 0;
	/* function obtains direction */
	virtual uint8_t get_direction(const Vector &start, const Vector &end) = 0;
	/* function returns the direction and index of point */
	virtual std::tuple<uint8_t, uint8_t, Index> get_position(const Vector &start, const Vector &end) = 0;
	virtual void iteration() = 0;
	double eval_source(const Vector& point);
public:
	Solver(double (*q)(double x, double y, double z), double sigma, double reflection, 
		   int N, int M, int L, int num_grids, int num_dirs, uint8_t geometry);

	void solve(int iters);
	void save(std::string file_name);
	void set_input_stream(std::string file_name);
	~Solver();
};

class SolverRect: public Solver {
	friend class Characteristic;
private:
	/* override virtual functions */
	uint8_t get_point(const Vector &vec);
	uint8_t get_direction(const Vector &start, const Vector &end);
	std::tuple<uint8_t, uint8_t, Index> get_position(const Vector &start, const Vector &end);
	void iteration();
public:
	SolverRect(double (*q)(double x, double y, double z), double sigma, double reflection, 
			   double a, double b, double c, int N, int M, int L);
};

class SolverHex: public Solver {
	friend class Characteristic;
private:
	/* override virtual functions */
	uint8_t get_point(const Vector &vec);
	uint8_t get_direction(const Vector &start, const Vector &end);
	std::tuple<uint8_t, uint8_t, Index> get_position(const Vector &start, const Vector &end);
	void iteration();
public:
	SolverHex(double (*q)(double x, double y, double z), double sigma, double reflection, 
			  double a, double b, int N, int M, int L);
};

#endif
