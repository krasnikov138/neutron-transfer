#include <cstring>
#include "characteristic.cpp"

/* implementation of abstract class Solver */

Solver::Solver(double (*q)(double x, double y, double z), double sigma, double reflection, 
               int N, int M, int L, int num_grids, int num_dirs, uint8_t geometry):
               q_ext(q), sigma(sigma), reflection(reflection), N(N), M(M), L(L),
               num_grids(num_grids), geometry(geometry) {
    /* initialize all grids */
    grid = new Grid [num_grids];

    reflection_matrix = new uint8_t [num_dirs];
    chars = new Characteristic [num_dirs];
}

double Solver::eval_source(const Vector& point) {
    Vector scaled_point;
    scaled_point.x = point.x * cell_size.a;
    scaled_point.y = point.y * cell_size.b;
    scaled_point.z = point.z * cell_size.c;

    return q_ext(scaled_point.x, scaled_point.y, scaled_point.z);
}

void Solver::solve(int iters = 1000) {
    for (int i = 0; i < iters; i++)
        iteration();
}

void Solver::save(std::string file_name) {
    std::ofstream output(file_name, std::ios::binary);

    /* write information which is neccessary for parsing */
    output.write(reinterpret_cast<char*>(&geometry), 1);
    output.write(reinterpret_cast<char*>(&num_grids), 4);
    output.write(reinterpret_cast<char*>(&cell_size), sizeof(CellSize));

    for (int g = 0; g < num_grids; g++) {
        output.write(reinterpret_cast<char*>(&grid[g].dimensions.x), 4);
        output.write(reinterpret_cast<char*>(&grid[g].dimensions.y), 4);
        output.write(reinterpret_cast<char*>(&grid[g].dimensions.z), 4);
        for (int i = 0; i < grid[g].dimensions.x; i++)
            for (int j = 0; j < grid[g].dimensions.y; j++)
                for (int k = 0; k < grid[g].dimensions.z; k++)
                    output.write(reinterpret_cast<char*>(grid[g].phi[i][j][k]), grid[g].num_dirs * sizeof(double));
    }
}

Solver::~Solver() {
    delete [] grid;
    delete [] reflection_matrix;
    delete [] chars;
}

/* only for test purposes */
void Solver::set_input_stream(std::string file_name) {
    Stream stream;
    int num_records;
    std::ifstream input_file(file_name);

    if (!input_file) 
        throw std::runtime_error("Can't open file with input streams!");

    input_file >> num_records;
    for (int i = 0; i < num_records; i++) {
        input_file >> stream;
        grid[stream.grid][stream.cell][stream.dir] = stream.value;
        input_streams[get_stream_index(stream.dir, stream.grid, stream.cell)] = stream.value;
    }
}

size_t Solver::get_stream_index(int dir_ind, int grid_ind, const Index& cell) {
    Grid *current_grid = &(grid[grid_ind]);
    return grid_ind * (N + 1) * (M + 1) * L * current_grid->num_dirs +
           cell.x * (M + 1) * L * current_grid->num_dirs + 
           cell.y * L * current_grid->num_dirs + 
           cell.z * current_grid->num_dirs + dir_ind;
}

/* implementation of class SolverRect for rectangular geometry */

SolverRect::SolverRect(double (*q)(double x, double y, double z), double sigma, 
                       double reflection = 0.5, double a = 1.0, double b = 1.0, double c = 1.0,
                       int N = 1, int M = 1, int L = 1):
                       Solver(q, sigma, reflection, N, M, L, 1, 56, 0) {
    cell_size.a = a; cell_size.b = b; cell_size.c = c;
    /* allocate memory for all grids */
    grid[0].allocate(Index(N, M, L), 30);

    /* description of reflecting directions
    0 1 2 3 4 5 6 7 8 9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29
    1 2 3 0 5 6 7 4 9 10 11  8 27 28 29 18 15 16 17 26 23 24 25 22 19 20 21 12 13 14
    */
    uint8_t temp_matrix[] = {1, 2, 3, 0, 5, 6, 7, 4, 9, 10, 
                             11, 8, 27, 28, 29, 18, 15, 16, 17, 
                             26, 23, 24, 25, 22, 19, 20, 21, 12, 13, 14};

    memcpy(reflection_matrix, temp_matrix, 30);

    Vector points[] = { {0.5, 0, 0}, {0, 0.5, 0}, {-0.5, 0, 0}, 
                        {0, -0.5, 0}, {0, 0, 0.5}, {0, 0, -0.5} };
    uint8_t dirs[15][2] = { {3, 0}, {0, 1}, {1, 2}, {2, 3}, {3, 4}, {4, 1},
                            {1, 5}, {5, 3}, {2, 4}, {4, 0}, {0, 5}, {5, 2}, 
                            {2, 0}, {3, 1}, {5, 4} };
    
    for (int i = 0; i < 15; i++)
        chars[i].offset = points[dirs[i][1]] - points[dirs[i][0]];
    for (int i = 15; i < 30; i++)
        chars[i].offset = points[dirs[i - 15][0]] - points[dirs[i - 15][1]];

    chars[12].linear = chars[13].linear = chars[14].linear = false;
    chars[27].linear = chars[28].linear = chars[29].linear = false;
}

uint8_t SolverRect::get_point(const Vector &vec) {
    uint8_t point;

    if (vec.x > 0.25)
        point = 0;
    else if (vec.x < -0.25)
        point = 2;
    else if (vec.y > 0.25)
        point = 1;
    else if (vec.y < -0.25)
        point = 3;
    else if (vec.z > 0.25)
        point = 4;
    else if (vec.z < -0.25)
        point = 5;

    return point;
}

uint8_t SolverRect::get_direction(const Vector &start, const Vector &end) {
    uint8_t start_point = get_point(start);
    uint8_t end_point = get_point(end);

    uint8_t mapping[6][6] = 
    {
        {255, 1, 27, 15, 24, 10},
        {16, 255, 2, 28, 20, 6},
        {12, 17, 255, 3, 8, 26},
        {0, 13, 18, 255, 4, 22},
        {9, 5, 23, 19, 255, 29},
        {25, 21, 11, 7, 14, 255}
    };
    return mapping[start_point][end_point];
}

std::tuple<uint8_t, uint8_t, Index> SolverRect::get_position(const Vector &start, const Vector &end) {
    Vector middle = 0.5 * (start + end);
    Index position;

    position.x = round(middle.x + 0.5 * (N - 1));
    position.y = round(middle.y + 0.5 * (M - 1));
    position.z = round(middle.z + 0.5 * (L - 1));

    Vector bias = {0.5 * (N - 1), 0.5 * (M - 1), 0.5 * (L - 1)};

    Vector centered_start = start - static_cast<Vector>(position) + bias;
    Vector centered_end = end - static_cast<Vector>(position) + bias;

    return std::make_tuple(get_direction(centered_start, centered_end), 0, position);
}

/* implementation of class SolverHex for hexogonal geometry */

SolverHex::SolverHex(double (*q)(double x, double y, double z), double sigma, 
                     double reflection = 0.5, double a = 1.0, double b = 1.0,
                     int N = 1, int M = 1, int L = 1):
                     Solver(q, sigma, reflection, N, M, L, 2, 56, 1) {
    cell_size.a = a; cell_size.b = a; cell_size.c = b;
    /* allocate memory for all grids */
    grid[0].allocate(Index(N + 1, M + 1, L), 56);
    grid[1].allocate(Index(N, M, L), 56);

    /* description of reflecting directions
    0 1 2 3 4 5 6 7 8 9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
    1 2 3 4 5 0 7 8 6 10 11  9 46 47 48 49 50 51 40 41 42 43 44 45 52 53 54 55
    --------------------------------------------------------------------------
    28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 
    33 28 29 30 31 32 36 34 35 39 37 38 15 16 17 12 13 14 21 22 23 18 19 20 24 25 26 27
    */
    uint8_t temp_matrix[] = {1, 2, 3, 4, 5, 0, 7, 8, 6, 10, 11, 9, 46, 47, 48, 49, 50,
                             51, 40, 41, 42, 43, 44, 45, 52, 53, 54, 55, 33, 28, 29, 30,
                             31, 32, 36, 34, 35, 39, 37, 38, 15, 16, 17, 12, 13, 14, 21, 
                             22, 23, 18, 19, 20, 24, 25, 26, 27};

    memcpy(reflection_matrix, temp_matrix, 56);

    Vector points[] = { {0.75, sqrt(3) / 4, 0}, {0, sqrt(3) / 2, 0},
                        {-0.75, sqrt(3) / 4, 0}, {-0.75, -sqrt(3) / 4, 0}, 
                        {0, -sqrt(3) / 2, 0}, {0.75, -sqrt(3) / 4, 0},
                        {0, 0, 0.5}, {0, 0, -0.5} };
    uint8_t dirs[28][2] = { {0, 1}, {1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 0}, 
                            {0, 2}, {2, 4}, {4, 0}, {1, 3}, {3, 5}, {5, 1},
                            {6, 0}, {6, 1}, {6, 2}, {6, 3}, {6, 4}, {6, 5},
                            {7, 0}, {7, 1}, {7, 2}, {7, 3}, {7, 4}, {7, 5},
                            {0, 3}, {1, 4}, {2, 5}, {6, 7} };
    
    for (int i = 0; i < 28; i++)
        chars[i].offset = points[dirs[i][1]] - points[dirs[i][0]];
    for (int i = 28; i < 56; i++)
        chars[i].offset = points[dirs[i - 28][0]] - points[dirs[i - 28][1]];
    chars[24].linear = chars[25].linear = chars[26].linear = chars[27].linear = false;
    chars[52].linear = chars[53].linear = chars[54].linear = chars[55].linear = false;
}

uint8_t SolverHex::get_point(const Vector &vec) {
    uint8_t point;

    if (vec.z > 0.25)
        point = 6;
    else if (vec.z < -0.25)
        point = 7;
    else if (vec.x > 0.25) {
        if (vec.y > 0)
            point = 0;
        else
            point = 5;
    }
    else if (vec.x < -0.25) {
        if (vec.y > 0)
            point = 2;
        else
            point = 3;
    }
    else if (vec.y > 0)
        point = 1;
    else
        point = 4;

    return point;
}

uint8_t SolverHex::get_direction(const Vector &start, const Vector &end) {
    uint8_t start_point = get_point(start);
    uint8_t end_point = get_point(end);

    uint8_t mapping[8][8] = 
    {
        {255, 0, 6, 24, 36, 33, 40, 46},
        {28, 255, 1, 9, 25, 39, 41, 47},
        {34, 29, 255, 2, 7, 26, 42, 48},
        {52, 37, 30, 255, 3, 10, 43, 49},
        {8, 53, 35, 31, 255, 4, 44, 50},
        {11, 54, 38, 32, 5, 255, 45, 51},
        {12, 13, 14, 15, 16, 17, 255, 27},
        {18, 19, 20, 21, 22, 23, 55, 255}
    };
    return mapping[start_point][end_point];
}

std::tuple<uint8_t, uint8_t, Index> SolverHex::get_position(const Vector &start, const Vector &end) {
    int z;
    int x0, y0;
    int x1, y1;
    double dist0, dist1;

    Vector middle = 0.5 * (start + end);
    Index position;

    z = round(middle.z + 0.5 * (L - 1));

    x0 = round(middle.x / 3 + 0.5 * N);
    y0 = round(middle.y / sqrt(3) + 0.5 * M);
 
    x1 = round(middle.x / 3 + 0.5 * (N - 1));
    y1 = round(middle.y / sqrt(3) + 0.5 * (M - 1));

    dist0 = pow(middle.x - 3 * (x0 - 0.5 * N), 2) + pow(middle.y - sqrt(3) * (y0 - 0.5 * M), 2);
    dist1 = pow(middle.x - 3 * (x1 - 0.5 * (N - 1)), 2) + pow(middle.y - sqrt(3) * (y1 - 0.5 * (M - 1)), 2);

    uint8_t grid;
    Vector bias, centered_start, centered_end;
    if (dist0 < dist1) {
        grid = 0;
        position = {x0, y0, z};
        bias = {0.5 * N, 0.5 * M, 0.5 * (L - 1)};
    }
    else {
        grid = 1;
        position = {x1, y1, z};
        bias = {0.5 * (N - 1), 0.5 * (M - 1), 0.5 * (L - 1)};
    }

    bias -= static_cast<Vector>(position);
    bias.x *= 3.0; bias.y *= sqrt(3);
    centered_start = start + bias;
    centered_end = end + bias;

    return std::make_tuple(get_direction(centered_start, centered_end), grid, position);
}

/* very boring functions for iterating through edges */

void SolverRect::iteration() {
    Stream stream;
    Vector start_point;
    std::array<uint8_t, 5> char_index;

    /* edge -1, 0, 0: iterate through all points */
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < L; j++) {
            start_point = {-0.5 * N, i - 0.5 * (M - 1), j - 0.5 * (L - 1)};
            char_index = {3, 8, 17, 26, 12};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }

    /* edge 1, 0, 0: iterate through all points */
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < L; j++) {
            start_point = {0.5 * N, i - 0.5 * (M - 1), j - 0.5 * (L - 1)};
            char_index = {1, 10, 15, 24, 27};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }

    /* edge 0, -1, 0: iterate through all points */
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < L; j++) {
            start_point = {i - 0.5 * (N - 1), -0.5 * M, j - 0.5 * (L - 1)};
            char_index = {0, 4, 13, 18, 22};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }

    /* edge 0, 1, 0: iterate through all points */
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < L; j++) {
            start_point = {i - 0.5 * (N - 1), 0.5 * M, j - 0.5 * (L - 1)};
            char_index = {2, 6, 16, 20, 28};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }

    /* edge 0, 0, -1: iterate through all points */
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            start_point = {i - 0.5 * (N - 1), j - 0.5 * (M - 1), -0.5 * L};
            char_index = {7, 11, 14, 21, 25};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }

    /* edge 0, 0, 1: iterate through all points */
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            start_point = {i - 0.5 * (N - 1), j - 0.5 * (M - 1), 0.5 * L};
            char_index = {5, 9, 19, 23, 29};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }
}

void SolverHex::iteration() {
    Stream stream;
    Vector start_point;
    std::array<uint8_t, 7> char_index;

    /* edge -1, 0, 0: iterate through all points */
    for (int i = 0; i < 2 * M; i++) {
        for (int j = 0; j < L; j++) {
            start_point = {-1.5 * N - 0.75, 0.5 * sqrt(3) * (double(i) - M + 0.5), j - 0.5 * (L - 1)};
            if (i % 2)
                char_index = {3, 10, 30, 37, 43, 49, 52};
            else
                char_index = {2, 7, 26, 29, 34, 42, 48};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }

    /* edge 1, 0, 0: iterate through all points */
    for (int i = 0; i < 2 * M; i++) {
        for (int j = 0; j < L; j++) {
            start_point = {1.5 * N + 0.75, 0.5 * sqrt(3) * (double(i) - M + 0.5), j - 0.5 * (L - 1)};
            if (i % 2)
                char_index = {5, 11, 32, 38, 45, 51, 54};
            else
                char_index = {0, 6, 24, 33, 36, 40, 46};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }

    /* edge 0, -1, 0: iterate through all points */
    for (int i = 0; i < 2 * (N + 1); i++) {
        for (int j = 0; j < L; j++) {
            start_point = {-1.5 * N - 0.75 + 1.5 * i, -sqrt(3) * (0.5 * M + 0.25), j - 0.5 * (L - 1)};
            if (i % 2)
                char_index = {5, 11, 32, 38, 45, 51, 54};
            else
                char_index = {3, 10, 30, 37, 43, 49, 52};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }
    /* grid 0 */
    for (int i = 0; i < N + 1; i++) {
        for (int j = 0; j < L; j++) {
            start_point = {-1.5 * N + 3 * i, -sqrt(3) * (0.5 * M + 0.5), j - 0.5 * (L - 1)};
            char_index = {4, 8, 31, 35, 44, 50, 53};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }
    /* grid 1 */
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < L; j++) {
            start_point = {-1.5 * (N - 1) + 3 * i, -sqrt(3) * 0.5 * M, j - 0.5 * (L - 1)};
            char_index = {4, 8, 31, 35, 44, 50, 53};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }

    /* edge 0, 1, 0: iterate through all points */
    for (int i = 0; i < 2 * (N + 1); i++) {
        for (int j = 0; j < L; j++) {
            start_point = {-1.5 * N - 0.75 + 1.5 * i, sqrt(3) * (0.5 * M + 0.25), j - 0.5 * (L - 1)};
            if (i % 2)
                char_index = {0, 6, 24, 33, 36, 40, 46};
            else
                char_index = {2, 7, 26, 29, 34, 42, 48};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }
    /* grid 0 */
    for (int i = 0; i < N + 1; i++) {
        for (int j = 0; j < L; j++) {
            start_point = {-1.5 * N + 3 * i, sqrt(3) * (0.5 * M + 0.5), j - 0.5 * (L - 1)};
            char_index = {1, 9, 25, 28, 39, 41, 47};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }
    /* grid 1 */
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < L; j++) {
            start_point = {-1.5 * (N - 1) + 3 * i, sqrt(3) * 0.5 * M, j - 0.5 * (L - 1)};
            char_index = {1, 9, 25, 28, 39, 41, 47};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }

    /* edge 0, 0, -1: iterate through all points */
    for (int i = 0; i < N + 1; i++) {
        for (int j = 0; j < M + 1; j++) {
            start_point = {3 * (-0.5 * N + i), sqrt(3) * (-0.5 * M + j), -0.5 * L};
            char_index = {18, 19, 20, 21, 22, 23, 55};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            start_point = {3 * (-0.5 * (N - 1) + i), sqrt(3) * (-0.5 * (M - 1) + j), -0.5 * L};
            char_index = {18, 19, 20, 21, 22, 23, 55};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }

    /* edge 0, 0, 1: iterate through all points */
    for (int i = 0; i < N + 1; i++) {
        for (int j = 0; j < M + 1; j++) {
            start_point = {3 * (-0.5 * N + i), sqrt(3) * (-0.5 * M + j), 0.5 * L};
            char_index = {12, 13, 14, 15, 16, 17, 27};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            start_point = {3 * (-0.5 * (N - 1) + i), sqrt(3) * (-0.5 * (M - 1) + j), 0.5 * L};
            char_index = {12, 13, 14, 15, 16, 17, 27};
            /* iterate through characteristics */
            for (auto &ch : char_index)
                stream = chars[ch].integrate(this, start_point);
        }
    }
}
