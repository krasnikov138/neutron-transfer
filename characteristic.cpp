#include <cmath>
#include "solver.h"

double approx_linear(double q_l, double q_r, double sigma, double delta) {
    double res = q_r - q_l;
    res += (1 - exp(-sigma * delta)) * (q_l - (q_r - q_l) / (sigma * delta));
    return res / sigma;
}

double approx_square(double q_l, double q_m, double q_r, double sigma, double delta) {
    double part1;
    double part2;
    part1 = q_r - q_l - 4 / (delta * sigma) * (q_l + q_r - 2 * q_m);
    part2 = q_l - (4 * q_m - q_r - 3 * q_l) / (sigma * delta);
    part2 += 4 * (q_r + q_l - 2 * q_m) / pow(sigma * delta, 2);
    return (part1 + (1 - exp(-sigma * delta)) * part2) / sigma;
}

Stream Characteristic::integrate(Solver* solver, const Vector& start_point) {
    /* some useful constants */
    double q_l, q_r, q_m, addition;
    double sigma = solver->sigma;
    double delta = offset.length();

    /* point where the values of external source function are obtained */
    Vector point = start_point + offset;

    /* index of current and next point */
    /* next point is the point where the distribution function is calculated */
    Index current_cell, next_cell;
    uint8_t current_direction, next_direction;
    uint8_t current_grid, next_grid;

    std::tie(current_direction, current_grid, current_cell) = solver->get_position(start_point, point);
    std::tie(next_direction, next_grid, next_cell) = solver->get_position(point, point + offset);

    int i = 0;
    q_l = solver->eval_source(start_point);

    while (next_cell.is_in_range((solver->grid)[next_grid].dimensions)) {
        q_r = solver->eval_source(point);
        if (linear)
            addition = approx_linear(q_l, q_r, sigma, delta);
        else {
            q_m = solver->eval_source(point - 0.5 * offset);
            addition = approx_square(q_l, q_m, q_r, sigma, delta);
        }

        (solver->grid[next_grid])[next_cell][next_direction] = 
            exp(-sigma * delta) * (solver->grid[current_grid])[current_cell][current_direction] + addition;

        /* go to the next point */      
        i++;
        q_l = q_r;
        current_cell = next_cell;
        current_grid = next_grid;
        current_direction = next_direction;
        point = start_point + (i + 1.0) * offset;

        std::tie(next_direction, next_grid, next_cell) = solver->get_position(point, point + offset);
    } 
    // find output stream for detremining reflection
    q_r = solver->eval_source(point);
    if (linear)
        addition = approx_linear(q_l, q_r, sigma, delta);
    else {
        q_m = solver->eval_source(point - 0.5 * offset);
        addition = approx_square(q_l, q_m, q_r, sigma, delta);
    }
    /* peform reflection */
    double input_value;
    double output_value;
    double reflected_value = exp(-sigma * delta) * 
           (solver->grid[current_grid])[current_cell][current_direction] + addition;

    uint8_t reflected_direction = solver->reflection_matrix[current_direction];
    auto it = solver->input_streams.find(solver->get_stream_index(reflected_direction, current_grid, current_cell));
    if (it != solver->input_streams.end())
        input_value = it->second;
    else
        input_value = 0.0;
    solver->grid[current_grid][current_cell][reflected_direction] = reflected_value * solver->reflection + input_value;

    output_value = (1 - solver->reflection) * reflected_value;
    /* check that the stream does not return to the substance */
    std::tie(next_direction, next_grid, next_cell) = solver->get_position(point + offset, point + 2.0 * offset);
    if (next_cell.is_in_range((solver->grid)[next_grid].dimensions))
        solver->input_streams[solver->get_stream_index(next_direction, next_grid, next_cell)] = output_value;

    return Stream(current_direction, current_grid, output_value, current_cell);
}
