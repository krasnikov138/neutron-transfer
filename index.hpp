#include <iostream>

#ifndef ABSTRACT_VECTOR_CLASS
#define ABSTRACT_VECTOR_CLASS

template <class T>
struct abstract_vector {
	T x, y, z;

	// constructors
	abstract_vector(T x = 0, T y = 0, T z = 0): x(x), y(y), z(z) {}
	abstract_vector(const abstract_vector& other): x(other.x), y(other.y), z(other.z) {}

	// assignment
	abstract_vector& operator=(const abstract_vector &);

	// addition
	abstract_vector operator+(const abstract_vector &) const;
	abstract_vector& operator+=(const abstract_vector &);

	// subtraction
	abstract_vector operator-(const abstract_vector &) const;
	abstract_vector& operator-=(const abstract_vector &);

	// multiplication by the constant
	abstract_vector operator*(T) const;
	abstract_vector& operator*=(T);

	template <class F>
	friend abstract_vector<F> operator*(F, const abstract_vector<F> &);

	// static casting from one template to another
	template <class F>
	operator abstract_vector<F>();

	// check whether the vector the area
	bool is_in_range(T, T, T);
	bool is_in_range(const abstract_vector &);

	double length();

	friend std::ostream& operator<< (std::ostream& output, const abstract_vector& vec) {
		output << vec.x << " " << vec.y << " " << vec.z;
		return output;
	}

	friend std::istream& operator>> (std::istream& input, abstract_vector& vec) {
		input >> vec.x >> vec.y >> vec.z;
		return input;
	}
};

template <class T>
abstract_vector<T>& abstract_vector<T>::operator=(const abstract_vector<T> &other) {
	this->x = other.x;
	this->y = other.y;
	this->z = other.z;
	return *this;
}

template <class T>
abstract_vector<T> abstract_vector<T>::operator+(const abstract_vector<T> &other) const {
	return abstract_vector(x + other.x, y + other.y, z + other.z);
}

template <class T>
abstract_vector<T>& abstract_vector<T>::operator+=(const abstract_vector<T> &other) {
	this->x += other.x;
	this->y += other.y;
	this->z += other.z;
	return *this;
}

template <class T>
abstract_vector<T> abstract_vector<T>::operator*(T c) const {
	return abstract_vector<T>(x * c, y * c, z * c);
}

template <class T>
abstract_vector<T>& abstract_vector<T>::operator*=(T c) {
	x *= c; y *= c; z *= c;
	return *this;
}

template <class T>
abstract_vector<T> abstract_vector<T>::operator-(const abstract_vector<T> &other) const {
	return abstract_vector<T>(x - other.x, y - other.y, z - other.z);
}

template <class T>
abstract_vector<T>& abstract_vector<T>::operator-=(const abstract_vector<T> &other) {
	this->x -= other.x;
	this->y -= other.y;
	this->z -= other.z;
	return *this;
}

template <class T>
abstract_vector<T> operator*(T c, const abstract_vector<T> &other) {
	return abstract_vector<T>(other.x * c, other.y * c, other.z * c);
}

template <class T>
template <class F>
abstract_vector<T>::operator abstract_vector<F>() {
	return abstract_vector<F>(static_cast<F>(x), static_cast<F>(y), static_cast<F>(z));
}

template <class T>
bool abstract_vector<T>::is_in_range(T X, T Y, T Z) {
	if (x >= 0 && x < X && y >= 0 && y < Y && z >= 0 && z < Z)
		return true;
	return false;
}

template <class T>
bool abstract_vector<T>::is_in_range(const abstract_vector<T>& other) {
	if (x >= 0 && x < other.x && 
		y >= 0 && y < other.y && 
		z >= 0 && z < other.z)
		return true;
	return false;
}

template <class T>
double abstract_vector<T>::length() {
	return sqrt(x * x + y * y + z * z);
}

typedef abstract_vector<int> Index;
typedef abstract_vector<double> Vector;

#endif
