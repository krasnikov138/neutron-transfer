#include "index.hpp"

#ifndef CHARACTERISTIC_CLASS
#define CHARACTERISTIC_CLASS

class Solver;

struct Stream {
	int dir;
	int grid;
	double value;
	Index cell;

	Stream(): 
		dir(0), grid(0), value(0.0), cell(Index(0, 0, 0)) {}

	Stream(int dir, int grid, double value, const Index& cell): 
		dir(dir), grid(grid), value(value), cell(cell) {}

	Stream& operator=(const Stream& other) {
		dir = other.dir;
		grid = other.grid;
		value = other.value;
		cell = other.cell;

		return *this;
	}

	friend std::ostream& operator<< (std::ostream& output, const Stream& obj) {
		output << obj.grid << " " << obj.cell << " " << obj.dir << obj.value;
		return output;
	}

	friend std::istream& operator>> (std::istream& input, Stream& obj) {
		input >> obj.grid >> obj.cell >> obj.dir >> obj.value;
		return input;
	}
};

/* description of characteristic */
struct Characteristic {
	bool linear;
	Vector offset;

	Characteristic():
		linear(true), offset(Vector()) {};
	Characteristic(bool linear, const Vector &offset):
		linear(linear), offset(offset) {};

	Stream integrate(Solver* solver, const Vector& start_point);
};

#endif
