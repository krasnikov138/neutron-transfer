import sys
import struct

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Line3DCollection

class Solution:
    def __init__(self):
        self.geometry = 'rect'
        self.num_dirs = 30
        self.N, self.M, self.L = 1, 1, 1
        self.a, self.b, self.c = 1.0, 1.0, 1.0
        self.grids = [np.zeros((1, 1, 1, 30, 3))]


    def __repr__(self):
        representation = f"size = [{self.N}, {self.M}, {self.L}]"
        if self.geometry == 'rect':
            representation = "RectSolution(" + representation 
            representation += f", cell = [{self.a:.2f}, {self.b:.2f}, {self.c:.2f}])"
        if self.geometry == 'hex':
            representation = "HexSolution(" + representation 
            representation += f", cell = [{self.a:.2f}, {self.c:.2f}])"
        return representation


    def load(self, fname):
        with open(fname, "rb") as f:
            # define geometry type
            self.geometry, = struct.unpack('b', f.read(1))
            if self.geometry == 0:
                self.geometry = 'rect'
                self.num_dirs = 30
            elif self.geometry == 1:
                self.geometry = 'hex'
                self.num_dirs = 56
            else:
                raise Exception(f"The geometry type {self.geometry} isn't supported.")

            # load information about cell size
            num_grids, = struct.unpack('i', f.read(4))
            self.a, self.b, self.c = struct.unpack('3d', f.read(24))
            if self.geometry == 'rect':
                assert num_grids == 1, f"Rectangular geometry must have only 1 grid, this file contains {num_grids}."
            if self.geometry == 'hex':
                assert num_grids == 2, f"Hexogonal geometry must have only 2 grid, this file contains {num_grids}."

            # load all grids
            self.grids = []
            for i in range(num_grids):
                self.N, self.M, self.L = struct.unpack('3i', f.read(12))
                count = self.N * self.M * self.L * self.num_dirs
                self.grids.append(np.fromfile(f, dtype='f8', count=count).reshape((self.N, self.M, self.L, self.num_dirs)))

            if self.geometry == 'hex':
                assert (self.grids[0].shape[0] == self.grids[1].shape[0] + 1) and \
                       (self.grids[0].shape[1] == self.grids[1].shape[1] + 1) and \
                       (self.grids[0].shape[2] == self.grids[1].shape[2]), \
                       f"Grid sizes are inconsistent: {self.grids[0].shape}, {self.grids[1].shape}"


    def info(self):
        print(f"Geometry: {self.geometry}")
        print(f"Number of directions: {self.num_dirs}")
        if self.geometry == 'rect':
            print(f"Cell sizes: {self.a} {self.b} {self.c}")
        else:
            print(f"Cell sizes: {self.a} {self.b}")
        print("Grids parameters: ")
        for i, grid in enumerate(self.grids):
            print(f"{i} - {grid.shape[0]} {grid.shape[1]} {grid.shape[2]}")


    def get_directions(self, grid):
        total_size = grid.shape[0] * grid.shape[1] * grid.shape[2]

        if self.geometry == 'rect':
            points = [[0.5, 0, 0], [0, 0.5, 0], [-0.5, 0, 0], 
                      [0, -0.5, 0], [0, 0, 0.5], [0, 0, -0.5]]
            dirs = [[3, 0], [0, 1], [1, 2], [2, 3], [3, 4], [4, 1],
                    [1, 5], [5, 3], [2, 4], [4, 0], [0, 5], [5, 2], 
                    [2, 0], [3, 1], [5, 4]]
            scale = np.array([self.a, self.b, self.c])
        elif self.geometry == 'hex':
            points = [[np.cos(np.pi / 3 * (i + 0.5)), np.sin(np.pi / 3 * (i + 0.5)), 0] 
                            for i in range(6)] + [[0, 0, 0.5], [0, 0, -0.5]]
            dirs = [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 0], 
                    [0, 2], [2, 4], [4, 0], [1, 3], [3, 5], [5, 1],
                    [6, 0], [6, 1], [6, 2], [6, 3], [6, 4], [6, 5],
                    [7, 0], [7, 1], [7, 2], [7, 3], [7, 4], [7, 5],
                    [0, 3], [1, 4], [2, 5], [6, 7]]
            scale = np.array([3 * self.a, np.sqrt(3) * self.b, self.c])

        # construct all directions (30 or 56)
        dirs = [[points[direction[0]], points[direction[1]]] for direction in dirs]
        dirs = np.array(dirs)
        opposite_dirs = np.array(list(map(lambda x: [x[1], x[0]], dirs)))
        dirs = np.concatenate([dirs, opposite_dirs])
        
        # scale directions according to cell size
        if self.geometry == 'rect':
            dirs *= np.array([self.a, self.b, self.c])
        elif self.geometry == 'hex':
            dirs *= np.array([self.a * np.sqrt(3) / 2, self.b * np.sqrt(3) / 2, self.c])
        
        # construct start points in the centers of grid
        start_points = dirs[:, 0, :]
        centers = np.array([[[[i - 0.5 * (grid.shape[0] - 1),
                               j - 0.5 * (grid.shape[1] - 1), 
                               k - 0.5 * (grid.shape[2] - 1)] for k in range(grid.shape[2])] 
                            for j in range(grid.shape[1])] for i in range(grid.shape[0])])
        centers = centers.repeat(self.num_dirs, axis=2).reshape((total_size * self.num_dirs, 3))
        start_points = np.tile(start_points, (total_size, 1))
        start_points += centers * scale
        
        # multiply the value of distribution function of the vector of the direction
        dirs = dirs[:, 1, :] - dirs[:, 0, :]
        dirs = dirs / np.linalg.norm(dirs, axis=1).repeat(3).reshape((self.num_dirs, 3))
        dirs = np.tile(dirs, (total_size, 1)).ravel()
        mas = grid.ravel().repeat(3, axis=0)
        dirs = (mas * dirs).reshape((total_size * self.num_dirs, 3))
        return start_points, dirs


    def plot_rect_cell(self, ax, center):
        points = np.array([[0.5, 0.5, 0.5], [-0.5, 0.5, 0.5], [-0.5 , -0.5, 0.5], [0.5, -0.5, 0.5],
                           [0.5, 0.5, -0.5], [-0.5, 0.5, -0.5],  [-0.5 , -0.5, -0.5], [0.5, -0.5, -0.5]])
        points *= np.array([self.a, self.b, self.c])
        points += center
        edges = [[0, 1], [1, 2], [2, 3], [3, 0], 
                 [4, 5], [5, 6], [6, 7], [7, 4],
                 [0, 4], [1, 5], [2, 6], [3, 7]]
        cell = [[points[point] for point in edge] for edge in edges]
        ax.add_collection3d(Line3DCollection(cell, linestyles='solid', color='k', linewidths=1.5))


    def plot_hex_cell(self, ax, center):
        points = np.array([[np.cos(np.pi / 3 * i), np.sin(np.pi / 3 * i), 0] for i in range(6)])
        points = np.concatenate([points - np.array([0, 0, 0.5]), points + np.array([0, 0, 0.5])])
        points *= np.array([self.a, self.b, self.c])
        points += center

        edges = [[0, 1, 2, 3, 4, 5, 0], [6, 7, 8, 9, 10, 11, 6],
                 [0, 6], [1, 7], [2, 8], [3, 9], [4, 10], [5, 11]]
        cell = [[points[point] for point in path] for path in edges]
        ax.add_collection3d(Line3DCollection(cell, linestyles='solid', color='k', linewidths=1.5))


    def plot(self, ax, scale=0.05):
        for grid in self.grids:
            points, dirs = self.get_directions(grid)
            ax.quiver(points[:, 0], points[:, 1], points[:, 2], 
                      dirs[:, 0], dirs[:, 1], dirs[:, 2], length=scale, linewidths=1.5)

        if self.geometry == 'rect':
            grid = self.grids[0]
            centers = np.array([[[[i - 0.5 * (grid.shape[0] - 1),
                                   j - 0.5 * (grid.shape[1] - 1), 
                                   k - 0.5 * (grid.shape[2] - 1)] for k in range(grid.shape[2])] 
                                for j in range(grid.shape[1])] for i in range(grid.shape[0])])
            centers *= np.array([self.a, self.b, self.c])
            centers = centers.reshape((grid.shape[0] * grid.shape[1] * grid.shape[2], 3))
            for center in centers:
                self.plot_rect_cell(ax, center)
        elif self.geometry == 'hex':
            for grid in self.grids:
                centers = np.array([[[[i - 0.5 * (grid.shape[0] - 1),
                                       j - 0.5 * (grid.shape[1] - 1), 
                                       k - 0.5 * (grid.shape[2] - 1)] for k in range(grid.shape[2])] 
                                    for j in range(grid.shape[1])] for i in range(grid.shape[0])])
                centers *= np.array([3 * self.a, np.sqrt(3) * self.b, self.c])
                centers = centers.reshape((grid.shape[0] * grid.shape[1] * grid.shape[2], 3))
                for center in centers:
                    self.plot_hex_cell(ax, center)
            

if __name__ == '__main__':
    # parsing of command line arguments
    i = 1
    args = {'scale': 0.05, 'xlim': 1.0, 'ylim': 1.0, 'zlim': 1.0}
    file_name = "result.out"
    while i < len(sys.argv):
        if sys.argv[i].startswith('--'):
            if i + 1 >= len(sys.argv):
                raise Exception(f"Argument {sys.argv[i]} is undefined.")
            else:
                args[sys.argv[i][2:]] = sys.argv[i + 1]
            i += 2
        else:
            file_name = sys.argv[i]
            i += 1
    args = {key: float(item) for key, item in args.items()}

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    solution = Solution()
    solution.load(file_name)
    solution.info()
    solution.plot(ax, args['scale'])
    
    ax.set_aspect('equal')
    ax.set_proj_type('ortho')
    ax.view_init(elev=90, azim=-90)
    ax.set_xlim([-args['xlim'], args['xlim']])
    ax.set_ylim([-args['ylim'], args['ylim']])
    ax.set_zlim([-args['zlim'], args['zlim']])
    plt.axis('off')
    plt.show()
