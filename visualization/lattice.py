import sys
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Line3DCollection
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection

def plot_hex_pattern(ax, a, b, center):
    """Plot 9 points of a hexagonal pattern."""
    pattern_points = np.array([[np.cos((i + 0.5) * np.pi / 3), np.sin((i + 0.5) * np.pi / 3), 0] 
                              for i in np.arange(6)]) * np.sqrt(3) / 2
    pattern_points = np.vstack([pattern_points, np.array([[0, 0, 0], [0, 0, -0.5], [0, 0, 0.5]])])
    pattern_points *= np.array([a, a, b])
    pattern_points = pattern_points[[0, 1, 2, 8, 7, 5, 4, 3, 6]]

    pattern_points += center
    ax.scatter(pattern_points[:, 0], pattern_points[:, 1], pattern_points[:, 2])    
    ax.scatter(center[0], center[1], center[2], marker='x', s=150)

    for i, point in enumerate(pattern_points):
        ax.text(point[0], point[1], point[2], str(i + 1), fontsize=15, color='b')


def plot_line(ax, begin, end, color):
    """Plot line with indents."""
    real_begin = begin - (end - begin) / 5
    real_end = end + (end - begin) / 5
    points = np.vstack([real_begin, real_end])
    ax.plot(points[:, 0], points[:, 1], points[:, 2], color=color)


def plot_hex_cell(ax, a, b, center):
    """
    Plot a hexagonal cell:
    a - bottom side, b - height
    center - center point of a cell.
    """
    points = np.array([[np.cos(phi), np.sin(phi), -0.5] for phi in np.pi / 3 * np.arange(6)] + 
                      [[np.cos(phi), np.sin(phi), 0.5] for phi in np.pi / 3 * np.arange(6)])
    points *= np.array([a, a, b])

    points += center
    edges = [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 0],
             [6, 7], [7, 8], [8, 9], [9, 10], [10, 11], [11, 6], 
             [0, 6], [1, 7], [2, 8], [3, 9], [4, 10], [5, 11]]

    cell = [[points[point] for point in edge] for edge in edges]

    ax.add_collection3d(Line3DCollection(cell, linestyles='dashed', color='k'))


def plot_rect_pattern(ax, a, b, c, center):
    """Plot 7 points of a rectangular pattern."""
    pattern_points = np.array([[0.5, 0, 0], [0, 0.5, 0], [-0.5, 0, 0],
                               [0, -0.5, 0], [0, 0, 0.5], [0, 0, -0.5], [0, 0, 0]])
    pattern_points *= np.array([a, b, c])
    pattern_points = pattern_points[[0, 1, 4, 5, 3, 2, 6]]

    pattern_points += center
    ax.scatter(pattern_points[:, 0], pattern_points[:, 1], pattern_points[:, 2])    
    ax.scatter(center[0], center[1], center[2], marker='x', s=150)

    for i, point in enumerate(pattern_points):
        ax.text(point[0], point[1], point[2], str(i + 1), fontsize=15, color='b')


def plot_rect_cell(ax, a, b, c, center):
    """
    Plot a rectangular cell:
    a - length, b - width, c - height
    center - center point of a cell.
    """
    points = np.array([[0.5, 0.5, 0.5], [-0.5, 0.5, 0.5], 
                       [-0.5, -0.5, 0.5], [0.5, -0.5, 0.5],
                       [0.5, 0.5, -0.5], [-0.5, 0.5, -0.5], 
                       [-0.5, -0.5, -0.5], [0.5, -0.5, -0.5]])
    points *= np.array([a, b, c])
    points += center
    edges = [[0, 1], [1, 2], [2, 3], [3, 0], 
             [4, 5], [5, 6], [6, 7], [7, 4],
             [0, 4], [1, 5], [2, 6], [3, 7]]
    cell = [[points[point] for point in edge] for edge in edges]

    ax.add_collection3d(Line3DCollection(cell, linestyles='dashed', color='k'))


def plot2d_hex_scheme(ax, a):
    points = a * np.array([[np.cos(phi), np.sin(phi)] for phi in np.pi / 3 * np.arange(6)])
    lines = [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 0]]
    centers = np.array([[np.cos(phi), np.sin(phi)] for phi in np.pi / 6 + np.pi / 3 * np.arange(6)])
    centers *= a * np.sqrt(3)
    centers = np.vstack([centers, np.array([0, 0])])

    pattern_points = centers / 2

    for pos in range(6):
        end = (pos + 1) % 6
        fp = 2.2 * (pattern_points[end] - pattern_points[pos]) + pattern_points[end]
        sp = -2.2 * (pattern_points[end] - pattern_points[pos]) + pattern_points[pos]
        p = np.vstack([sp, fp])
        ax.plot(p[:, 0], p[:, 1], color='#5500aa80')

        end = (pos + 2) % 6
        fp = 1.2 * (pattern_points[end] - pattern_points[pos]) + pattern_points[end]
        sp = -1.2 * (pattern_points[end] - pattern_points[pos]) + pattern_points[pos]
        p = np.vstack([sp, fp])
        ax.plot(p[:, 0], p[:, 1], color='#55aa0080')

        end = (pos + 3) % 6
        fp = 1.2 * (pattern_points[end] - pattern_points[pos]) + pattern_points[end]
        sp = -1.2 * (pattern_points[end] - pattern_points[pos]) + pattern_points[pos]
        p = np.vstack([sp, fp])
        ax.plot(p[:, 0], p[:, 1], color='#55aaff80')

    for center in centers:
        cell = [[(points + center)[point] for point in line] for line in lines]
        ax.add_collection(LineCollection(cell, linestyles='dashed', color='k'))
        ax.scatter((pattern_points + center)[:, 0], (pattern_points + center)[:, 1], c='r')
    ax.scatter(pattern_points[:, 0], pattern_points[:, 1], c='b')

    for i, point in zip([1, 2, 3, 8, 7, 6, 9], pattern_points):
        ax.text(point[0], point[1], str(i), fontsize=20)


def view_hex1(ax, a, b):
    centers = np.array([[0, 0, 0], [1.5, -np.sqrt(3) / 2, 0],
                        [1.5, np.sqrt(3) / 2, 0], [0, np.sqrt(3), 0]])
    centers *= np.array([a, a, b])
    # plot 4 hexagonal cells
    for center in centers:
        plot_hex_cell(ax, a, b, center)
    plot_hex_pattern(ax, a, b, centers[0])

    main_point = np.array([0, -np.sqrt(3) / 2 * a, 0])
    intersect_points = np.array([[1.5, np.sqrt(3), 0], [1.5, 0, 0],
                                 [2.25, np.sqrt(3) / 4, 0], [0.75, np.sqrt(3) / 4, 0],
                                 [0, 1.5 * np.sqrt(3), 0], [0, np.sqrt(3), 0]])
    intersect_points *= np.array([a, a, b])

    # plot intersection points
    ax.scatter(intersect_points[:, 0], intersect_points[:, 1],
               intersect_points[:, 2], color='r')

    # plot neutron directions
    plot_line(ax, main_point, intersect_points[0], color='#5500aa')
    plot_line(ax, main_point, intersect_points[2], color='#5500aa')
    plot_line(ax, main_point, intersect_points[4], color='#5500aa')
    plot_line(ax, main_point, np.array([-0.75 * a, np.sqrt(3) / 4 * a, 0]), color='#5500aa')
    plot_line(ax, main_point, np.array([-0.75 * a, -np.sqrt(3) / 4 * a, 0]), color='#5500aa')


def view_hex2(ax, a, b):
    centers = np.array([[0, 0, 0], [0, 0, 1],
                        [0, 0, -1], [0, np.sqrt(3), 0],
                        [0, np.sqrt(3), 1], [0, np.sqrt(3), -1]])
    centers *= np.array([a, a, b])    
    # plot 4 hexagonal cells
    for center in centers:
        plot_hex_cell(ax, a, b, center)
    plot_hex_pattern(ax, a, b, centers[0])

    main_point = np.array([0, -np.sqrt(3) / 2 * a, 0])
    intersect_points = np.array([[0, 0, 0.5], [0, np.sqrt(3) / 2, 1],
                                 [0, np.sqrt(3), 1.5], [0, 0, 0.5],
                                 [0, np.sqrt(3) / 2, -1], [0, np.sqrt(3), -1.5],
                                 [0, np.sqrt(3), 0], [0, 3 * np.sqrt(3) / 2, 0],
                                 [0, 2 * np.sqrt(3), 0], [0, 0, 1],
                                 [0, 0, 1.5], [0, 0, -1],
                                 [0, 0, -1.5]])
    intersect_points *= np.array([a, a, b])
    # plot intersection points
    ax.scatter(intersect_points[:, 0], intersect_points[:, 1],
               intersect_points[:, 2], color='r')
    plot_line(ax, main_point, intersect_points[2], color='#5500aa')
    plot_line(ax, main_point, intersect_points[5], color='#5500aa')
    plot_line(ax, main_point, intersect_points[8], color='#5500aa')
    plot_line(ax, np.array([0, 0, -1.5 * b]), np.array([0, 0, 1.5 * b]), color='#5500aa')


def view_rect1(ax, a, b, c):
    centers = np.array([[0, 0, 0], [1.0, 0, 0], [1.0, 1.0, 0],
                        [0, 1.0, 0], [-1.0, 1.0, 0], [-1.0, 0, 0]])
    centers *= np.array([a, b, c])
    # plot 4 hexagonal cells
    for center in centers:
        plot_rect_cell(ax, a, b, c, center)
    plot_rect_pattern(ax, a, b, c, centers[0])

    main_point = np.array([0, -b / 2, 0])
    intersect_points = np.array([[1, 0.5, 0], [1.5, 1, 0],
                                 [-1, 0.5, 0], [-1.5, 1, 0],
                                 [0, 1, 0], [0, 1.5, 0],
                                 [1, 0, 0], [1.5, 0, 0],
                                 [-1, 0, 0], [-1.5, 0, 0]])
    intersect_points *= np.array([a, b, c])

    # plot intersection points
    ax.scatter(intersect_points[:, 0], intersect_points[:, 1],
               intersect_points[:, 2], color='r')
    plot_line(ax, main_point, intersect_points[1], color='#5500aa')
    plot_line(ax, main_point, intersect_points[3], color='#5500aa')

    plot_line(ax, main_point, intersect_points[5], color='#55aaff')
    plot_line(ax, centers[0], intersect_points[7], color='#55aaff')
    plot_line(ax, centers[0], intersect_points[9], color='#55aaff')


def view_rect2(ax, a, b, c):
    centers = np.array([[0, 0, 0], [0, 1.0, 0], [0, 1.0, 1.0],
                        [0, 1.0, -1.0], [0, 0, 1.0], [0, 0, -1.0]])
    centers *= np.array([a, b, c])
    # plot 4 hexagonal cells
    for center in centers:
        plot_rect_cell(ax, a, b, c, center)
    plot_rect_pattern(ax, a, b, c, centers[0])

    main_point = np.array([0, -b / 2, 0])
    intersect_points = np.array([[0, 0.5, 1], [0, 1, 1.5],
                                 [0, 0.5, -1], [0, 1, -1.5],
                                 [0, 1, 0], [0, 1.5, 0],
                                 [0, 0, 1], [0, 0, 1.5],
                                 [0, 0, -1], [0, 0, -1.5]])
    intersect_points *= np.array([a, b, c])

    # plot intersection points
    ax.scatter(intersect_points[:, 0], intersect_points[:, 1],
               intersect_points[:, 2], color='r')
    plot_line(ax, main_point, intersect_points[1], color='#5500aa')
    plot_line(ax, main_point, intersect_points[3], color='#5500aa')

    plot_line(ax, main_point, intersect_points[5], color='#55aaff')
    plot_line(ax, centers[0], intersect_points[7], color='#55aaff')
    plot_line(ax, centers[0], intersect_points[9], color='#55aaff')


if __name__ == '__main__':
    rect = True
    view = 1
    # parse command line arguments
    i = 1
    while i < len(sys.argv):
        if sys.argv[i] == '--rect':
            rect = True
        elif sys.argv[i] == '--hex':
            rect = False
        elif sys.argv[i] == '--view':
            if i + 1 < len(sys.argv):
                view = int(sys.argv[i + 1])
                i += 1
            else:
                raise Exception("There is no view number.")
        else:
            raise Exception(f"Wrong arguments: {sys.argv[i]}")
        i += 1


    fig = plt.figure()
    if view < 3:
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlim([-1.6, 1.6])
        ax.set_ylim([-1.6, 1.6])
        ax.set_zlim([-1.6, 1.6])
    else:
        ax = fig.add_subplot(111)
        ax.set_xlim([-3, 3])
        ax.set_ylim([-3, 3])

    if rect:
        a, b, c = 1.5, 2.5 / 2, 1
        if view == 1:
            view_rect1(ax, a, b, c)
        elif view == 2:
            view_rect2(ax, a, b, c)
        else:
            raise Exception("Possible view values for rect geometry are 1 and 2.")
    else:
        a = b = 1
        if view == 1:
            view_hex1(ax, a, b)
        elif view == 2:
            view_hex2(ax, a, b)
        elif view == 3:
            plot2d_hex_scheme(ax, a)
        else:
            raise Exception("Possible view values for hex geometry are 1, 2 and 3.")

    ax.set_aspect('equal')
    plt.axis('off')

    plt.show()
