import sys

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Line3DCollection

from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

import matplotlib.pyplot as plt


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
        self._verts3d = xs, ys, zs


    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        FancyArrowPatch.draw(self, renderer)


class Arrow:
    def __init__(self, start, finish):
        direction = finish - start
        self.finish = start + 0.5 * direction / np.linalg.norm(direction)
        self.start = start


    def plot(self, ax, text):     
        points = np.vstack([self.start, self.finish])
        arrow = Arrow3D(points[:, 0], points[:, 1], points[:, 2], 
                        mutation_scale=15, lw=2, arrowstyle="->", color="r")
        ax.add_artist(arrow)
        ax.text(self.finish[0], self.finish[1], self.finish[2], text, fontsize=15, color="b")


def plot_rect_cell(ax, a, b, c, center):
    """
    Plot a rectangular cell:
    a - length, b - width, c - height
    center - center point of a cell.
    """
    points = np.array([[0.5, 0.5, 0.5], [-0.5, 0.5, 0.5], 
                       [-0.5 , -0.5, 0.5], [0.5, -0.5, 0.5],
                       [0.5, 0.5, -0.5], [-0.5, 0.5, -0.5], 
                       [-0.5 , -0.5, -0.5], [0.5, -0.5, -0.5]])
    points *= np.array([a, b, c])

    center_face = np.array([[0.5, 0, 0], [-0.5, 0, 0],
                            [0, 0.5, 0], [0, -0.5, 0],
                            [0, 0, 0.5], [0, 0, -0.5]])
    center_face *= np.array([a, b, c])

    points += center
    center_face += center

    edges = [[0, 1], [1, 2], [2, 3], [3, 0], 
             [4, 5], [5, 6], [6, 7], [7, 4],
             [0, 4], [1, 5], [2, 6], [3, 7]]
    cell = [[points[point] for point in edge] for edge in edges]

    connections = np.array([[[i, j] for j in range(6) if j > i] for i in range(6)])
    flat_connections = []
    for connection_list in connections:
        flat_connections.extend(connection_list)
    connections = flat_connections

    directions = [[center_face[point] for point in connection] for connection in connections]

    ax.add_collection3d(Line3DCollection(cell, linestyles='solid', color='k'))
    ax.add_collection3d(Line3DCollection(directions, linestyles='dashed', color='k'))


def plot_hex_cell(ax, a, b, center):
    points = [[np.cos(np.pi / 3 * i), np.sin(np.pi / 3 * i), 0] for i in range(6)]
    points = np.array(points)
    points = np.concatenate([points - np.array([0, 0, 0.5]), 
                             points + np.array([0, 0, 0.5])])
    points *= np.array([a, a, b])
    points += center

    edges = [[0, 1, 2, 3, 4, 5, 0], [6, 7, 8, 9, 10, 11, 6],
             [0, 6], [1, 7], [2, 8], [3, 9], [4, 10], [5, 11]]
    cell = [[points[point] for point in path] for path in edges]

    center_face = [[np.cos(np.pi / 3 * (i + 0.5)), np.sin(np.pi / 3 * (i + 0.5)), 0] 
                        for i in range(6)] + [[0, 0, 0.5], [0, 0, -0.5]]
    center_face = np.array(center_face) * np.array([a * np.sqrt(3) / 2, a * np.sqrt(3) / 2, b])
    connections = np.array([[[i, j] for j in range(8) if j > i] for i in range(8)])
    flat_connections = []
    for connection_list in connections:
        flat_connections.extend(connection_list)
    connections = flat_connections

    directions = [[center_face[point] for point in connection] for connection in connections]

    ax.add_collection3d(Line3DCollection(cell, linestyles='solid', color='k'))
    ax.add_collection3d(Line3DCollection(directions, linestyles='dashed', color='k'))


if __name__ == "__main__":
    # parsing arguments
    mode = "rect"
    if len(sys.argv) > 1:
        mode = sys.argv[1]
    assert mode in ["rect", "hex"], f"Wrong mode {mode}. Possible values are: rect and hex."

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    if mode == "rect":
        a, b, c = 2, 2, 2
        plot_rect_cell(ax, a, b, c, np.zeros(3))

        # define directions for arrows
        points = [[0.5, 0, 0], [0, 0.5, 0], [-0.5, 0, 0], 
                  [0, -0.5, 0], [0, 0, 0.5], [0, 0, -0.5]]
        dirs = [[3, 0], [0, 1], [1, 2], [2, 3], [3, 4], [4, 1],
                [1, 5], [5, 3], [2, 4], [4, 0], [0, 5], [5, 2], 
                [2, 0], [3, 1], [5, 4]]
        scale = np.array([a, b, c])
        
    elif mode == "hex":
        a, b, = 1.5, 1.5
        plot_hex_cell(ax, a, b, np.zeros(3))

        points = [[np.cos(np.pi / 3 * (i + 0.5)), np.sin(np.pi / 3 * (i + 0.5)), 0] 
                            for i in range(6)] + [[0, 0, 0.5], [0, 0, -0.5]]
        dirs = [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 0], 
                [0, 2], [2, 4], [4, 0], [1, 3], [3, 5], [5, 1],
                [6, 0], [6, 1], [6, 2], [6, 3], [6, 4], [6, 5],
                [7, 0], [7, 1], [7, 2], [7, 3], [7, 4], [7, 5],
                [0, 3], [1, 4], [2, 5], [6, 7]]
        scale = np.array([a * np.sqrt(3) / 2, a * np.sqrt(3) / 2, b])

    dirs = [[points[direction[0]], points[direction[1]]] for direction in dirs]
    dirs = np.array(dirs)
    opposite_dirs = np.array(list(map(lambda x: [x[1], x[0]], dirs)))
    dirs = np.concatenate([dirs, opposite_dirs])
    dirs *= scale

    arrows = [Arrow(direction[0], direction[1]) for direction in dirs]

    for num, arrow in enumerate(arrows):
        arrow.plot(ax, str(num))

    ax.set_aspect('equal')
    ax.set_xlim([-0.9, 0.9])
    ax.set_ylim([-0.9, 0.9])
    ax.set_zlim([-0.9, 0.9])
    plt.axis('off')
    plt.show()
