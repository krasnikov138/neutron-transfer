В данном разделе произведен переход от непрерывного интегро-дифференциального уравнения
\eqref{stationary-form} и граничного условия \eqref{reflection-cond} к их 
дискретным аналогам. Дано описание шаблона схемы для случаев 
прямоугольной и шестиугольной геометрий и аналитически выведены коэффициенты 
дискретного уравнения.

\subsection{Фундаментальное решение}
Построить наиболее точный дискретный аналог уравнения \eqref{stationary-form} 
можно на основе так называемого фундаментального решения. Записывая 
\textbf{характеристическую} форму кинетического уравнения через оператор 
дифференцирования вдоль траектории 
$\frac{d}{dl} = \frac{1}{v} \frac{\partial}{\partial t} + \frac{\partial}{\partial l}$,
получаем следующее равенство:
\begin{equation} \label{trans-eq-char}
	\frac{d \varphi}{d l} + \Sigma \varphi = q(\varphi) + q^{ext}
\end{equation}

Проинтегрируем полученное уравнение вдоль траектории нейтрона. 
Тогда вышеприведенное соотношение примет вид:
\begin{multline}\label{formal-solution}
	\varphi(E, \mathbf{\Omega}, l, t) = \varphi_1\left(E, \mathbf{\Omega}, l_1, t - \frac{l - l_1}{v} \right)
	\exp\left[ -\int\limits_{l_1}^l \Sigma(E, l', t')dl' \right] + \\
	\int\limits_{l_1}^{l}[q(E, \mathbf{\Omega}, l', t') + q^{ext}(E, \mathbf{\Omega}, l', t')]
	\exp\left[ -\int\limits_{l'}^l \Sigma(E, l'', t'')dl''\right]dl'
\end{multline}
где $t'(l') = t - \frac{l-l'}{v}$ и $t''(l'') = t - \frac{l-l''}{v}$ --- 
запаздывающие времена. В нестационарной задаче нужно брать именное такое время, 
так как пучок нейтронов, испущенный из точки $l'$ достигнет точки $l$ за время $
\Delta t_{ll'} = \frac{l-l'}{v}$. Следовательно, начальный и конечный моменты 
времени также должны отличаться на $\Delta t_{ll'}$. 
Следует отметить, что соотношение \eqref{formal-solution} не является окончательным
решением --- оно представляет собой интегральное уравнение относительно
$\varphi(E, \mathbf{\Omega}, \mathbf{r}, t)$ 
(собственный источник $q$ зависит от $\varphi$). Для решения интегрального уравнения 
также необходимо учитывать граничные условия отражения \eqref{boundary-cond} и 
\eqref{reflection-cond} на поверхности области.

Соотношение \eqref{formal-solution} допускает простую физическую интерпретацию. 
Изначально мы считаем, что распределение нейтронов в точке $l_1$ известно и 
задается функцией $\varphi_1(E, \mathbf{\Omega}, l_1, t)$. 
Первое слагаемое описывет затухание этого пучка при переходе в точку $l$ 
(коэффициент затухания $\exp\left[ -\int\limits_{l_1}^l \Sigma(E, l', t')dl' \right]$). 
Второе слагаемое представляет собой вклад в пучок от всех внешних и собственных источников 
$l'$ на пути от $l_1$ к $l$ (поэтому производится интегрирование по $l'$). 
Однако, каждый такой вклад затухает в
$\exp\left[ -\int\limits_{l'}^l \Sigma(E, l'', t'')dl''\right]$ раз, 
что объясняет наличие экспоненты во втором слагаемом. 
Замена времени $t$ на запаздывающие $t'$ и $t''$ была обоснована выше.

Несмотря на ясную физическую трактовку \eqref{formal-solution}, 
полезно убедиться в нем с помощи непосредственной подстановки в \eqref{trans-eq-char}. 
Произведя дифференцирование по \eqref{formal-solution} $l$, имеем:

\begin{multline*}
	\frac{\partial \varphi}{\partial l} = -\exp\left[ -\int\limits_{l_1}^l \Sigma(E, l', t')dl' \right] 
	\left( \frac{1}{v}\frac{d\varphi_1}{dt} + \varphi_1\left\{ \Sigma(E, l, t) - 
	\frac{1}{v} \int\limits_{l_1}^l \frac{d}{dt'}\Sigma(E, l', t')dl' \right\} \right) - \\
	\int\limits_{l_1}^l[q(E, \mathbf{\Omega}, l', t') + q^{ext}(E, \mathbf{\Omega}, l', t')]
	\exp\left[ -\int\limits_{l'}^l \Sigma(E, l'', t'')dl''\right] \left\{ \Sigma(E, l, t) - \frac{1}{v} 
	\int\limits_{l'}^l \frac{d}{dt''}\Sigma(E, l'', t'')dl'' \right\} dl' - \\
	\frac{1}{v}\int\limits_{l_1}^l \frac{d}{dt'} [q+q^{ext}](E, \mathbf{\Omega}, l', t') 
	\exp\left[ -\int\limits_{l'}^l \Sigma(E, l'', t'')dl''\right]dl' + 
	q(E, \mathbf{\Omega}, l, t) + q^{ext}(E, \mathbf{\Omega}, l, t)
\end{multline*}

Теперь по $t$:
\begin{multline*}
	\frac{1}{v}\frac{\partial \varphi}{\partial t} = 
	\exp\left[ -\int\limits_{l_1}^l \Sigma(E, l', t')dl' \right] 
	\left( \frac{1}{v}\frac{d\varphi_1}{dt} - 
	\frac{1}{v} \varphi_1\left(E, \mathbf{\Omega}, l_1, t - \frac{l - l_1}{v} \right) 
	\int\limits_{l_1}^l \frac{d}{dt'}\Sigma(E, l', t')dl' \right) - 
	\\
	\int\limits_{l_1}^l \frac{1}{v}[q(E, \mathbf{\Omega}, l', t') + q^{ext}(E, \mathbf{\Omega}, l', t')]
	\exp\left[ -\int\limits_{l'}^l \Sigma(E, l'', t'')dl''\right] 
	\int\limits_{l'}^l \frac{d}{dt''}\Sigma(E, l'', t'')dl'' dl + 
	\\
	\frac{1}{v}\int\limits_{l_1}^l \frac{d}{dt'} [q(E, \mathbf{\Omega}, l', t')+q^{ext}(E, \mathbf{\Omega}, l', t')] 
	\exp\left[ -\int\limits_{l'}^l \Sigma(E, l'', t'')dl''\right]dl'
\end{multline*}

Вычисляя сумму $\frac{1}{v}\frac{\partial \varphi}{\partial t} + 
	\frac{\partial \varphi}{\partial l}$ 
(которая на самом деле представляет собой производные, взятые по верхнему 
пределу интегрирования $l$ в формуле \eqref{formal-solution}) находим:
\begin{multline*}
	\frac{1}{v}\frac{\partial \varphi}{\partial t} + \frac{\partial \varphi}{\partial l} =
	-\exp\left[ -\int\limits_{l_1}^l \Sigma(E, l', t')dl' \right] 
	\varphi_1\left(E, \mathbf{\Omega}, l_1, t - \frac{l - l_1}{v} \right) \Sigma(E, l, t) - 
	\\
	\int\limits_{l_1}^l[q(E, \mathbf{\Omega}, l', t') + q^{ext}(E, \mathbf{\Omega}, l', t')]
	\exp\left[ -\int\limits_{l'}^l \Sigma(E, l'', t'')dl''\right] \Sigma(E, l, t) + 
	\\
	q(E, \mathbf{\Omega}, l, t) + q^{ext}(E, \mathbf{\Omega}, l, t)
\end{multline*}
Нетрудно увидеть, что данное выражение есть сумма источников $q(\varphi)+q^{ext}$ за
вычетом выражения \eqref{formal-solution} для функции распределения, 
умноженной на $\Sigma(E, l, t)$. Таким образом, мы доказали, что \eqref{formal-solution} 
есть формальное решение интегро-дифференциального уравнения \eqref{trans-eq-char}.

Приведем для справки выражение для формального решения в терминах 
\textbf{оптической длины пути} 
$\tau(E, \mathbf{\Omega}, l, t) = 
	\int\limits_{l_0}^l \Sigma\left(E, l', t - \frac{l-l'}{v}\right)dl'$. 
Используя данное обозначение, формулу \eqref{formal-solution} можно записать в таком 
компактном виде:
\[
	\varphi(E, \mathbf{\Omega}, l, t) = \varphi_{1}
	\left(\mathbf{\Omega}, l_1, t - \frac{l-l_1}{v}\right) 
	e^{-\tau+\tau_1} + \int\limits_{l_1}^l[q(E, \mathbf{\Omega}, l', t') 
			+ q^{ext}(E, \mathbf{\Omega}, l', t')]e^{-\tau + \tau'}dl'
\]

\subsection{Дискретизация объекта}
В данной работе рассмотрено два способа разбиения объекта на правильные ячейки:
\begin{itemize}
	\item с помощью прямоугольных параллепипедов с параметрами $(a, b, c)$ --- такой 
	геометрией можно пользоваться при решении задачи радиационной защиты;
	\item посредством шестигранных призм со стороной основания $a$ и высотой $b$ --- 
	данный способ удобен для расчете активной зоны реактора.
\end{itemize}
Будем считать, что одна ячейка заполнена однородным материалом --- то есть задается 
кусочно-постоянная аппроксимация макросечения взаимодействия $\Sigma(E, \mathbf{r}, t)$ 
по пространственным координатам. Помимо этого нам необходимо определиться, для каких 
направлений $\mathbf{\Omega}$ мы будем хранить значение функции 
$\varphi(E, \mathbf{\Omega}, \mathbf{r}, t)$, иными словами выполнить дискретизацию по 
углам. Для этого вначале введем понятие \textit{минимального шаблона ячейки}. 
Шаблон ячейки --- это набор характерных точек на гранях вместе с центральной точкой 
ячейки. Ребром при этом называется отрезок, соединяющий две точки шаблона. 
Существует такой замечательный класс шаблонов, называемый \textit{минимальным}. 
Он состоит из центральной точки и центров граней, а все вершины в нём соединены друг с 
другом ребрами.

Очевидным достоинством схемы на минимальном шаблоне является то, что траектория, 
проходящая вдоль ребра шаблона, при пересечении соседних ячеек не образует дополнительных 
точек. Такие характеристики можно продолжать во всем объеме (здесь и далее в работе под 
характеристикой понимается \textit{траектория нейтрона}). При расширении же шаблона будут 
возникать новые вершины, что несколько усложнить схему.

Рассмотрим подробнее шаблон на основе прямоугольных параллелепипедов. 
Так как любые два центра граней соединены ребрами, на каждую ячейку приходится ровно
$2C_6^2=30$ характеристик (каждое ребро определяет сразу 2 направления --- в одну и 
другую сторону). Продолжив их для до пересечения с внешней границой  
объема, мы получим совокупность траекторий, для которых и будет строиться 
конечно--аналитическая схема. На рисунках \ref{rect_view1} и \ref{rect_view2} приведены 
примеры вершин шаблона и соединяющих их характеристик.

\begin{figure}[h]
	\centering{\includegraphics[width=160mm]{rect_view1.png}}
	\caption{Ячейка минимального шаблона в случае разбиения на прямоугольные 
	параллелепипеды. На рисунке показаны вершины шаблона 1--7, а также приведены примеры 
	траекторий, параллельных плоскости $xy$. \textcolor{red}{Красным} цветом выделены 
	точки пересечения характеристик с соседними ячейками.} 
	\label{rect_view1}
\end{figure}

\begin{figure}[h]
	\centering{\includegraphics[width=100mm]{rect_view2.png}}
	\caption{Тот же шаблон, что и на рисунке~\ref{rect_view1}, изображенный 
	под другим ракурсом. Наклонными линиями изображены траетории нейтронов, 
	выходящие из плоскости $xy$.}
	\label{rect_view2}
\end{figure}

% определим 2 цвета, которые понадобятся нам в дальнейшем
\definecolor{purple}{rgb}{0.32, 0, 0.67}
\definecolor{light-green}{rgb}{0.85, 1.0, 0.4}

Следует отметит, что цвета направлений на схемах \ref{rect_view1} и \ref{rect_view2} 
выбраны не случайно: \textcolor{cyan}{голубым} (II тип) отмечены те направления, вдоль 
которых производится квадратичная интерполяция функции распределения 
$\varphi(E, \mathbf{\Omega}, \mathbf{r}, t)$, а \textcolor{purple}{фиолетовый} (I тип) 
указывает на ребра, где возможна только лишь линейная. Это понадобится нам в дальнейшем 
при записи дискретного уравнения. В таблицах \ref{rect-dirs-table} и \ref{hex-dirs-table}
показаны различные свойства характеристик, такие как длина в пределах одной ячейки, 
направление и тип (I или II). Для экономии места здесь приведены траектории от 
меньшего индекса к большему, остальные же восстанавливаются с помощью следующего 
очевидного равенства: 
$\mathbf{\Omega}_{j \rightarrow i} = - \mathbf{\Omega}_{i \rightarrow j}$.
\begin{table}[h]
\begin{center}
\caption{Направления вектора $\mathbf{\Omega}$ в шаблоне на основе параллепипедов.}
\label{rect-dirs-table}
\begin{tabular}{|c|c|c|c|}
	\hline
	характеристика & тип & направление $\mathbf{\Omega}$ & длина \\ [1.5ex]
	\hline
	\hline
	$1 \rightarrow 5, 2 \rightarrow 6$ & \textcolor{purple}{I} & 
		$\left( -\frac{a}{\sqrt{a^2+b^2}}, -\frac{b}{\sqrt{a^2+b^2}}, 0\right)^T$ & 
		$\frac{1}{2}\sqrt{a^2+b^2}$ \\ [2.0ex]
	\hline
	$1 \rightarrow 2, 5 \rightarrow 6$ & \textcolor{purple}{I} & 
		$\left( -\frac{a}{\sqrt{a^2+b^2}}, \frac{b}{\sqrt{a^2+b^2}}, 0\right)^T$ & 
		$\frac{1}{2}\sqrt{a^2+b^2}$ \\ [2.0ex]
	\hline
	$1 \rightarrow 3, 4 \rightarrow 5$ & \textcolor{purple}{I} & 
		$\left( -\frac{a}{\sqrt{a^2+c^2}}, 0, \frac{c}{\sqrt{a^2+c^2}} \right)^T$ & 
		$\frac{1}{2}\sqrt{a^2+c^2}$ \\ [2.0ex]
	\hline
	$2 \rightarrow 3, 4 \rightarrow 6$ & \textcolor{purple}{I} & 
		$\left( 0, -\frac{b}{\sqrt{b^2+c^2}}, \frac{c}{\sqrt{b^2+c^2}}\right)^T$ & 
		$\frac{1}{2}\sqrt{b^2+c^2}$ \\ [2.0ex]
	\hline
	$3 \rightarrow 5, 2 \rightarrow 4$ & \textcolor{purple}{I} & 
		$\left( 0, -\frac{b}{\sqrt{b^2+c^2}}, -\frac{c}{\sqrt{b^2+c^2}}\right)^T$ & 
		$\frac{1}{2}\sqrt{b^2+c^2}$ \\ [2.0ex]
	\hline
	$3 \rightarrow 6, 1 \rightarrow 4$ & \textcolor{purple}{I} & 
		$\left( -\frac{a}{\sqrt{a^2+c^2}}, 0, -\frac{c}{\sqrt{a^2+c^2}} \right)^T$ & 
		$\frac{1}{2}\sqrt{a^2+c^2}$ \\ [2.0ex]
	\hline
	\hline
	$1 \rightarrow 6$ & \textcolor{cyan}{II} & $\left( -1, 0, 0 \right)^T$ & $a$ \\ [2.0ex]
	\hline
	$2 \rightarrow 5$ & \textcolor{cyan}{II} & $\left( 0, -1, 0 \right)^T$ & $b$ \\ [2.0ex]
	\hline
	$3 \rightarrow 4$ & \textcolor{cyan}{II} & $\left( 0, 0, -1 \right)^T$ & $c$ \\ [2.0ex]
	\hline
\end{tabular}
\end{center}
\end{table}

Теперь приступим к разбиению объекта на ячейки в форме правильных шестиугольных призм. 
В этом случае шаблон ячейки состоит из 9 точек. Общее количество ребер, соединяющих
вершины шаблона дается выражением: $C_8^7 = 28$ --- в итоге 56 направлений 
дискретизации по углам, что почти вдвое больше, чем при использовании прямоугольных 
параллелепипедов. Главное отличие этой схемы от предыдущей состоит в том, что вдоль 
некоторых направлений I-ого типа можно улучшить точность вычисления интегралов от 
источника $q(E, \mathbf{\Omega}, l)$, используя его значения в ближайших к 
рассматриваемой характеристике точках. К примеру, если взять прямую $1 \rightarrow 7$, 
то по точкам 6, 9 можно оценить функцию распределения в середине ребра: 
$\varphi_{middle} = \frac{\varphi_1+\varphi_7}{2}$. По значениям 
$\varphi_1, \varphi_{middle}, \varphi_7$ строится параболическая интерполяция 
$\varphi$ на отрезке 1--7. Аналогичные построения можно выполнить для ребер 
3--7, 2--8 и т. д. Направления с таким свойством будем называть направлением 
\textit{III типа} и обознать \colorbox{light-green}{зеленым} цветом.

На рисунке \ref{hex_view} показаны примеры характеристик и их пересечение с ближайшими 
ячейками. При этом видно, что в шаблоне не возникает новых точек. 
\begin{figure}[h]
	\begin{minipage}{0.4875\textwidth}
		\centering{\includegraphics[width=87.75mm]{hex_cell1.png}}
	\end{minipage}
	\begin{minipage}{0.5125\textwidth}
		\centering{\includegraphics[width=92.25mm]{hex_cell2.png}}
	\end{minipage}
	\caption{Минимальный шаблон в случае шестиугольной ячейки. 
	Центр шаблона --- точка 9, точка 1--8 --- центры граней призмы. 
	Направляющие проведены через вершину \textbf{7}.} 
	\label{hex_view}
\end{figure}

\begin{table}[h]
\begin{center}
\caption{Характеристики $\mathbf{\Omega}$ для правильного шестиугольного шаблона.}
\label{hex-dirs-table}
\begin{tabular}{|c|c|c|c|}
	\hline
	характеристика & тип & направление $\mathbf{\Omega}$ & длина \\ [1.5ex]
	\hline
	\hline
	$1 \rightarrow 2, 7 \rightarrow 8$ & \textcolor{purple}{I} & 
		$\left( -\frac{\sqrt{3}}{2}, \frac{1}{2}, 0\right)^T$ & 
		$\frac{\sqrt{3}}{2}a$ \\
	\hline
	$2 \rightarrow 3, 6 \rightarrow 7$ & \textcolor{purple}{I} & 
		$\left( -\frac{\sqrt{3}}{2}, -\frac{1}{2}, 0\right)^T$ & 
		$\frac{\sqrt{3}}{2}a$ \\
	\hline
	$3 \rightarrow 8, 1 \rightarrow 6$ & \textcolor{purple}{I} & 
		$\left( 0, -1, 0\right)^T$ & $\frac{\sqrt{3}}{2}a$ \\ [2.0ex]
	\hline
	$1 \rightarrow 4, 5 \rightarrow 8$ & \textcolor{purple}{I} & 
		$\left( -\frac{3a}{2\sqrt{3a^2+b^2}}, 
				-\frac{\sqrt{3}a}{2\sqrt{3a^2+b^2}}, 
				\frac{b}{\sqrt{3a^2+b^2}}\right)^T$ & 
		$\frac{1}{2}\sqrt{3a^2+b^2}$ \\ [2.0ex]
	\hline
	$2 \rightarrow 4, 5 \rightarrow 7$ & \textcolor{purple}{I} & 
		$\left( 0, -\frac{\sqrt{3}a}{\sqrt{3a^2+b^2}}, 
					\frac{b}{\sqrt{3a^2+b^2}}\right)^T$ & 
		$\frac{1}{2}\sqrt{3a^2+b^2}$ \\ [2.0ex]
	\hline
	$3 \rightarrow 4, 5 \rightarrow 6$ & \textcolor{purple}{I} & 
		$\left( \frac{3a}{2\sqrt{3a^2+b^2}}, 
				-\frac{\sqrt{3}a}{2\sqrt{3a^2+b^2}}, 
				\frac{b}{\sqrt{3a^2+b^2}}\right)^T$ & 
		$\frac{1}{2}\sqrt{3a^2+b^2}$ \\ [2.0ex]
	\hline
	$4 \rightarrow 6, 3 \rightarrow 5$ & \textcolor{purple}{I} & 
		$\left( \frac{3a}{2\sqrt{3a^2+b^2}}, 
				-\frac{\sqrt{3}a}{2\sqrt{3a^2+b^2}}, 
				-\frac{b}{\sqrt{3a^2+b^2}}\right)^T$ & 
		$\frac{1}{2}\sqrt{3a^2+b^2}$ \\ [2.0ex]
	\hline
	$4 \rightarrow 7, 2 \rightarrow 5$ & \textcolor{purple}{I} & 
		$\left(0, -\frac{\sqrt{3}a}{\sqrt{3a^2+b^2}}, 
				  -\frac{b}{\sqrt{3a^2+b^2}}\right)^T$ & 
		$\frac{1}{2}\sqrt{3a^2+b^2}$ \\ [2.0ex]
	\hline
	$4 \rightarrow 8, 1 \rightarrow 5$ & \textcolor{purple}{I} & 
		$\left( -\frac{3a}{2\sqrt{3a^2+b^2}}, 
				-\frac{\sqrt{3}a}{2\sqrt{3a^2+b^2}}, 
				-\frac{b}{\sqrt{3a^2+b^2}}\right)^T$ & 
		$\frac{1}{2}\sqrt{3a^2+b^2}$ \\ [2.0ex]
	\hline
	\hline
	$1 \rightarrow 8$ & \textcolor{cyan}{II} & 
		$\left( -\frac{\sqrt{3}}{2}, -\frac{1}{2}, 0\right)^T$ & 
		$\sqrt{3}a$ \\ [2.0ex]
	\hline
	$3 \rightarrow 6$ & \textcolor{cyan}{II} & 
		$\left( \frac{\sqrt{3}}{2}, -\frac{1}{2}, 0\right)^T$ & 
		$\sqrt{3}a$ \\ [2.0ex]
	\hline
	$2 \rightarrow 7$ & \textcolor{cyan}{II} & 
		$\left( 0, -1, 0\right)^T$ & $\sqrt{3}a$ \\ [2.0ex]
	\hline
	$4 \rightarrow 5$ & \textcolor{cyan}{II} & 
		$\left( 0, 0, -1\right)^T$ & $b$ \\ [2.0ex]
	\hline
	\hline
	$1 \rightarrow 3, 6 \rightarrow 8$ & \colorbox{light-green}{III} & 
		$\left( -1, 0, 0\right)^T$ & $\frac{3}{2}a$ \\ [2.0ex]
	\hline
	$1 \rightarrow 7, 2 \rightarrow 8$ & \colorbox{light-green}{III} & 
		$\left( -\frac{1}{2}, -\frac{\sqrt{3}}{2}, 0\right)^T$ & 
		$\frac{3}{2}a$ \\ [2.0ex]
	\hline
	$2 \rightarrow 6, 3 \rightarrow 7$ & \colorbox{light-green}{III} & 
		$\left( \frac{1}{2}, -\frac{\sqrt{3}}{2}, 0\right)^T$ & 
		$\frac{3}{2}a$ \\ [2.0ex]
	\hline
\end{tabular}
\end{center}
\end{table}

\subsection{Вычисление коэффициентов дискретного уравнения}
% особое внимание уделить этому разделу!!!
После того, как мы определились с разбиениями функции распределения $\varphi$ по 
пространственным координанатам $\mathbf{r}$ и угловым компонентам $\mathbf{\Omega}$, 
можно записать дискретный аналог уравнения переноса для задачи \eqref{stationary-form}. 
В данной работе мы построим \textit{конечно-аналитическую характеристическую схему}. Для
этого мы воспользуемся фундаментальным решением \eqref{formal-solution} для получения 
значений функции распределения $\varphi(\mathbf{\Omega}, l)$ вдоль характеристики. 
В нашем случае все характеристики пересекают объект в одних и тех же точках шаблона, то
есть они являются \textit{длинными}. 

Конечно--разностные схемы аппроксимации дифференциального оператора 
$\frac{d}{dl} = \frac{1}{v} \frac{\partial}{\partial t} + \frac{\partial}{\partial l}$ 
после прохождения нескольких ячеек размывают первоначально прямой пучок нейтронов по 
направлениям полета \cite{vladimirov-numeric}. Эффект искусственной дифракции пучка 
значительно снижается в схемах коротких характеристик и полностью устраняется в схемах 
длинных характеристик. Схемы длинных характеристик бывают конечно--разностными 
(конечные разности по характеристической переменной) и конечно-аналитическими. В типичной 
конечно--аналитической схеме с длинными характеристиками затухание пучка нейтронов 
подчиняется правильному экспоненциальному закону. Вместе с тем, источники нейтронов 
аппроксимируются кусочно--постоянной функцией на ячейках. Это нарушает баланс числа 
столкновений (число приходов --- число уходов нейтронов из пучка). Если схема построена 
правильно, то нарушения баланса числа столкновений имеют локальный характер ---
ограничивается размерами ячейки. Если схема построена неправильно, то нарушения баланса 
носят глобальный характер. Для локализации (уменьшения) искажений локального баланса 
размеры ячеек делают относительно небольшими, но большими, чем в разностных схемах. 
Построенное в работе дискретное кинетическое уравнение (схема) использует 
кусочно--линейную и кусочно--квадратичную аппроксимацию источников на совокупности 
пространственных ячеек. Это, как мы надеемся, позволит существенно снизить неточность 
локального баланса по числу столкновений и общую ошибку расчета, либо даст возможность 
проводить расчеты на ячейках большого размера при сохранении того же уровня точности.

Построенное ниже дискретное уравнение (схема) имеет следующие свойства:
\begin{itemize}
	\item Отсутствует искусственная дифракция пучка.
	\item Вне зависимости от размеров ячеек решение дискретного уравнения точно 
	воспроизводит решение уравнения переноса нейтронов, если коэффициенты есть 
	кусочно--постоянные, а источники нейтронов --- кусочно--линейные функции на
	совокупности ячеек, и почти точно, если источники кусочно--квадратичны.
	\item При уменьшении размеров ячеек решение дискретного уравнения быстро стремится к
	точному решению.
	\item Использование ячеек правильной формы позволяет свести к минимуму число длинных
	характеристик, что снижает объем вычислений и повышает эффективность программы.
	\item Дискретное уравнение можно применять для выполнения поточечных расчетов 
	энергетического спектра нейтронов, расчетов методом лебеговского осреднения спектров и
	с помощью многогруппового приближения.
\end{itemize}
 
Недостатком схемы является то, что не всегда можно выполнить дискретизацию объекта 
с помощью правильных ячеек.

Перейдем к построению схемы. Вначале запишем дискретный аналог для краевых условий 
\eqref{reflection-cond} в предположении, что отражение зеркальное:
\begin{equation}\label{boundary-cond-scheme}
\varphi_{ref[i]} = \varphi_{ref[i]}^{ent} + \rho \varphi_i
\end{equation}
где $ref[i]$ обозначает индекс пучка, полученного после отражения $i$--ого от поверхности
образца, $\varphi^{ent}$ --- функция распределения внешних пучков нейтронов.
Далее разобъем траекторию на отрезки 
$[l_{i-1}, l_i]$, где $i=1, \dots, n$, $n$ --- число ячеек вдоль заданного направления, 
а $l_i$ --- характеристические координаты точек пересечения границ ячеек. 
Записывая для каждого отрезка соотношение \eqref{formal-solution} получаем:
\begin{equation}\label{diff-scheme}
\begin{split}
	\varphi_i(E, \mathbf{\Omega}) = \alpha_i\varphi_{i-1}(E, \mathbf{\Omega}) 
		& + \beta_i, i=1, \dots, n \\
	\alpha_i = \exp{\left[-\int\limits_{l_{i-1}}^{l_i} \Sigma(E, l')dl'\right]} 
		& \approx e^{-\Sigma_i(E)[l_i - l_{i-1}]} \\
	\beta_i = \int\limits_{l_{i-1}}^{l_i} q(E, \mathbf{\Omega}, l')
		\exp{\left[-\int\limits_{l'}^{l_i}\Sigma(E, l'')dl''\right]dl'} 
		&\approx \int\limits_{l_{i-1}}^{l_i} q(E, \mathbf{\Omega}, l)
			e^{-\Sigma_i(E)[l_i-l_{i-1}]}dl
\end{split}
\end{equation}
$\alpha_i(q)$ и $\beta_i(q)$ - коэффициенты схемы. Вычислим их в двух приближениях: 
для линейной интерполяции источника (I тип характеристик) и для параболической (II тип). 
В линейном случае, значение источника вдоль ребра $[l_{i-1}, l_i]$ выражается через
$q(l_{i-1})$ и $q(l_i)$ по формуле:
\begin{equation}\label{linear-interpolation}
	q(l) = \frac{q(l_{i-1})(l_i - l) - q(l_i)(l - l_{i-1})}{l_i - l_{i-1}}, l_{i-1} \le l \le l_i
\end{equation}
Подставив аппроксимацию \eqref{linear-interpolation} в выражение для $\beta_i$, 
а также введя обозначения 
$\Delta_{i}~=~l_i~-~l_{i-1}$, $q(l_{i-1}) = q_{i-1}, q(l_i) = q_i$ получим:
\begin{multline*}
	\beta_i = \int\limits_{l_{i-1}}^{l_i} \left\{\frac{q(l_{i-1})(l_i - l)e^{-\Sigma_i(l_i - l)}}{l_i - l_{i-1}} 
	+ \frac{q(l_i)(l - l_{i-1})e^{-\Sigma_i(l_i - l)}}{l_i - l_{i-1}}\right\}dl = \\
	= \frac{1}{\Delta_i}\int \limits_0^{\Delta_i} 
		\left[q(l_{i-1})(\Delta_i - x)e^{-\Sigma_i(\Delta_i - x)} 
			+ q(l_i)xe^{-\Sigma_i(\Delta_i - x)}\right]dx = 
	\frac{1}{\Delta_i} \int \limits_0^{\Delta_i} e^{-\Sigma_i(\Delta_i - x)} 
		[q_{i-1}\Delta_i - xq_{i-1} + xq_{i}]dx = \\
	= \frac{1}{\Delta_i \Sigma_i} \int \limits_0^{\Delta_i} 
		[q_{i-1}\Delta_i - xq_{i-1} + xq_i]de^{-\Sigma(\Delta_i - x)} = 
		\frac{1}{\Delta_i \Sigma_i} \left\{ \Delta_i q_i - \Delta_i q_{i-1} e^{-\Sigma_i \Delta_i} - 
			\int \limits_0^{\Delta_i} e^{-\Sigma_i(\Delta_i - x)}(q_i - q_{i-1})dx\right\} \\
	= \frac{1}{\Sigma_i\Delta_i} \left\{ q_i \Delta_i - q_{i-1} \Delta_i e^{-\Sigma_i\Delta_i} - 
		\frac{1}{\Sigma_i}(q_i - q_{i-1})\left[1-e^{\Sigma_i\Delta_i}\right]\right\}
\end{multline*}
Полученная формула легко преобразуется к виду:
\begin{equation}\label{beta-linear}
	\beta_i = \frac{q_i - q_{i-1}}{\Sigma_i} + \frac{1-e^{\Sigma_i\Delta_i}}{\Sigma_i}
	\left[q_{i-1} - \frac{q_i - q_{i-1}}{\Sigma_i\Delta_i}\right]
\end{equation}
Именно она будет использоваться в программной реализации дискретного уравнения.

Теперь перейдем к ребрам II и III типов, где источник $q(l)$ на ребре $[l_{i-1}, l_i]$ 
приближается с помощью значений на концах $q(l_{i-1}) = q_{i-1}, q(l_i) = q_i$ 
и в середине ребра $q(l_{i-\frac{1}{2}}) = q_{i-\frac{1}{2}}$. 
Представим $q(l)$ в виде многочлена $q(l)=a(l-l_{i-1})^2 + b(l-l_{i-1}) + q_{i-1}$ и 
найдем коэффициенты $a, b$ из системы уравнений:
\begin{equation*}
\begin{cases}
	q_{i-\frac{1}{2}} = a\frac{\Delta_i^2}{4} + b\frac{\Delta_i}{2} + q_{i-1} \\
	q_i = a \Delta_i^2 + b \Delta_i + q_{i-1} \\
\end{cases}
\Longrightarrow
\begin{cases}
	2q_{i-\frac{1}{2}} = a\frac{\Delta_i^2}{2} + b\Delta_i + 2q_{i-1} \\
	q_i = a \Delta_i^2 + b \Delta_i + q_{i-1} \\
	4q_{i-\frac{1}{2}} = a\Delta_i^2 + 2b\Delta_i + 4q_{i-1}
\end{cases}
\Longrightarrow
\begin{cases}
	q_i - 2q_{i-1} = a\frac{\Delta_i^2}{2} - q_{i-1} \\
	4q_{i-\frac{1}{2}} - q_i = b\Delta_i + 3q_{i-1}
\end{cases}
\end{equation*}
Выражая $a$ и $b$, получим:
\begin{equation}\label{sqr_interpolation_coef}
\begin{split}
	a = \frac{2}{\Delta_i^2} \left\{ q_i + q_{i-1} - 2q_{i-\frac{1}{2}}\right\} \\
	b = \frac{1}{\Delta_i}\left\{4q_{i-\frac{1}{2}} - q_i - 3q_{i-1}\right\}
\end{split}
\end{equation}
Для определения коэффициента $\beta_i$ необходимо взять интеграл 
$\int\limits_0^{\Delta_i}(ax^2 + bx + q_{i-1})e^{-\Sigma_i(\Delta_i - x)}dx$. 
Дважды интегрируя по частям мы получим такое выражение:
\begin{equation*}
	\beta_i = \frac{a\Delta_i^2 + b\Delta_i}{\Sigma_i} - \frac{2a\Delta_i}{\Sigma_i^2} + 
		(1 - e^{-\Sigma_i \Delta_i})\left[\frac{q_{i-1}}{\Sigma_i} - 
		\frac{b}{\Sigma_i^2} + \frac{2a}{\Sigma_i^3}\right]
\end{equation*} 
Подстановка $a$ и $b$ в данную формулу и несложные преобразования дадут окончательный 
ответ:
\begin{multline}\label{beta-sqr}
	\beta_i = \left[\frac{q_i - q_{i-1}}{\Sigma_i} - \frac{4}{\Sigma_i^2\Delta_i}
	(q_i + q_{i-1} - 2q_{i-\frac{1}{2}})\right] + \\
	\frac{1-e^{\Sigma_i\Delta_i}}{\Sigma_i}
	\left(q_{i-1} - \frac{4q_{i-\frac{1}{2}} - q_i - 3q_{i-1}}{\Sigma_i\Delta_i} 
		+ \frac{4(q_i + q_{i-1} - 2q_{i-\frac{1}{2}})}{\Sigma_i^2\Delta_i^2} \right)
\end{multline}

Совокупность формул \eqref{boundary-cond-scheme}, \eqref{diff-scheme}, 
\eqref{beta-linear} и \eqref{beta-sqr} полностью 
задают дискретную схему для уравнения переноса \eqref{stationary-form} на всех трех типах 
характеристик. Однако, необходимо помнить, что сам источник 
$q(E, \mathbf{\Omega}, \mathbf{r})$ может зависеть 
от $\varphi(E, \mathbf{\Omega}, \mathbf{r})$. В этом случае задача 
\eqref{diff-scheme} решается итеративно: первоначально все $\varphi_i^0$ 
(0 --- номер итерации) инициализируются нулями, а затем при известном источнике 
$q(\varphi^k)$ вычисляется решение на $k + 1$ итерации. Источник $q(\varphi^k)$ 
считается по распределению $\varphi^k$, заданному в точках шаблона, на 
основе \textit{квадратурных формул}.

\subsection{Алгоритм вычисления решения}
Опишем в целом алгоритм решения задачи на электронно--вычислительной машине,
не вникая с технические детали реализации программного кода. Последовательность 
действий при одной итерации выглядит следующим образом:
\begin{enumerate}
	\item Инициализируем значения $\varphi$ нулями и задаем падающие на образец 
	внешние потоки нейтронов;
	\item Проходимся по всем точкам шаблона, лежащим на поверхности образца и 
	запускаем характеристики для всех направлений $\mathbf{\Omega}$;
	\item По формулам \eqref{diff-scheme}, \eqref{beta-linear} и \eqref{beta-sqr}
	выполняем переход от значений $\varphi_{i-1}$ к $\varphi_i$, до тех пор, пока 
	характеристика не выйдет за пределы области;
	\item При достижении границы находим функцию распределения отраженного луча 
	из краевого условия \eqref{boundary-cond-scheme};
	\item После обхода всех направлений и точек поверхности, производим перерасчет 
	источников по квадратурным формулам.
\end{enumerate}
Несмотря на то, что алгоритм имеет довольно простой вид, он имеет подводные 
камни, которые необходимо учитывать при программной реализации. В частности, трудность
вызывает вопрос эффективного хранения в памяти компьютера функции распределения нейтронов.
Структуры данных необходимо организовать таким образом, чтобы переход от величины 
$\varphi_{i-1}$ к $\varphi_i$ не было вычислительно сложной операцией. 
Предложенное мною решение для этой проблемы и описание архитектуры кода содержится в 
следующем разделе.

