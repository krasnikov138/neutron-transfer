#include "characteristic.h"

#ifndef GRID_CLASS
#define GRID_CLASS

class Grid {
	friend class Solver;
	friend class SolverRect;
	friend class SolverHex;
	friend class Characteristic;
private:
	Index dimensions;
	int num_dirs;

	double ****phi;
public:
	Grid(): num_dirs(0) {};
	Grid(const Index &dimensions, int num_dirs);
	void allocate(const Index &dimensions, int num_dirs);

	double* operator[](const Index& ind) {
		return phi[ind.x][ind.y][ind.z];
	}
	
	~Grid();
};

Grid::Grid(const Index &dimensions, int num_dirs) {
	allocate(dimensions, num_dirs);
}

void Grid::allocate(const Index &dimensions, int num_dirs){
	this->dimensions = dimensions;
	this->num_dirs = num_dirs;

	/* allocate memory and full all vertex with zeros */
	phi = new double*** [dimensions.x];
	for (int i = 0; i < dimensions.x; i++) {
		phi[i] = new double** [dimensions.y];
		for (int j = 0; j < dimensions.y; j++) {
			phi[i][j] = new double* [dimensions.z];
			for (int k = 0; k < dimensions.z; k++) {
				phi[i][j][k] = new double [num_dirs];
				for (int p = 0; p < num_dirs; p++)
					phi[i][j][k][p] = 0.0;
			}
		}
	}
}

Grid::~Grid() {
	if (num_dirs) {
		for (int i = 0; i < dimensions.x; i++) {
			for (int j = 0; j < dimensions.y; j++) {
				for (int k = 0; k < dimensions.z; k++)
					delete [] phi[i][j][k];
				delete [] phi[i][j];
			}
			delete [] phi[i];
		}
		delete [] phi;
	}		
}

#endif
