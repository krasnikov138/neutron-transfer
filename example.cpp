#include <iostream>
#include <cmath>

#include "solver.cpp"

double source1(double x, double y, double z) {
	return 0;
}

double source2(double x, double y, double z) {
	return 1;
}

/* 
parameters of the rectangular solver:
	source_function
	sigma
	reflection
	a b c
	N M L

parameters of the hexogonal solver:
	source_function
	sigma
	reflection
	a b
	N M L
*/

int main(int argc, char **argv) {
	bool rect = true;
	int iters = 1000;

	double sigma = 1.0;
	double reflection = 0.5;
	int grid[3] = {3, 3, 3};
	double cell[3] = {1.0, 1.0, 1.0};

	int test = 1;

	/* parsing arguments of command line */

	for (int i = 1; i < argc; i++) {
		std::string arg(argv[i]);
		if (arg == "--hex" || arg == "-h")
			rect = false;
		else if (arg == "--iter" || arg == "-i") {
			if (i + 1 < argc) {
				i++;
				iters = std::stoi(argv[i]);
			} else
				throw std::runtime_error("There is no value for iteration.");
		} else if (arg == "--test" || arg == "-t") {
			if (i + 1 < argc) {
				i++;
				test = std::stoi(argv[i]);
			} else
				throw std::runtime_error("There is no test number.");
		} else if (arg == "--sigma" || arg == "-s") {
			if (i + 1 < argc) {
				i++;
				sigma = std::stod(argv[i]);
			} else
				throw std::runtime_error("There is no value for sigma.");
		} else if (arg == "--ref" || arg == "-r") {
			if (i + 1 < argc) {
				i++;
				reflection = std::stod(argv[i]);
			} else
				throw std::runtime_error("There is no value for reflection.");
		} else if (arg == "--grid" || arg == "-g") {
			int start = i + 1;
			while (++i < argc && argv[i][0] != '-' && (i - start < 3))
				grid[i - start] = std::stoi(argv[i]);
			if (i - start != 3)
				throw std::runtime_error("There is some problem with grid sizes parsing.");
			i--;
		} else if (arg == "--cell" || arg == "-c") {
			int start = i + 1;
			while (++i < argc && argv[i][0] != '-' && (i - start < 3))
				cell[i - start] = std::stod(argv[i]);
			if (i - start < 1)
				throw std::runtime_error("There is some problem with cell parsing.");
			i--;
		} else
			throw std::runtime_error("Unknow argument: " + arg);
	}

	if (test != 1 && test != 2)
		throw std::runtime_error("Wrong test number, possible values are 1 or 2.");


	/* set source function */
	double (*source)(double, double, double);
	if (test == 1)
		source = source1;
	else
		source = source2;

	if (rect) {
		SolverRect rect(source, sigma, reflection, 
						cell[0], cell[1], cell[2], 
						grid[0], grid[1], grid[2]);
		rect.set_input_stream(std::string("tests/input_stream_rect.txt"));
		rect.solve(iters);
		rect.save(std::string("tests/result.out"));
	} else {
		SolverHex hex(source, sigma, reflection, 
					  cell[0], cell[1], 
					  grid[0], grid[1], grid[2]);
		hex.set_input_stream(std::string("tests/input_stream_hex.txt"));
		hex.solve(iters);
		hex.save(std::string("tests/result.out"));
	}

	return 0;
}
